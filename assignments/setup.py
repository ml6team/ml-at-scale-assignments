from setuptools import find_packages
from setuptools import setup

setup(
    name='ml-at-scale-assignments',
    version='0.1',
    author='Robbe Sneyders',
    author_email='robbe.sneyders@ml6.eu',
    install_requires=['tensorflow==1.8.0','tensorflow-transform==0.6.0',
                      'Pillow==5.0.0', 'numpy==1.14.0',
                      'apache-beam[gcp]==2.4.0'],
    packages=find_packages(),
    description='ML at scale boilerplate code'
)
