import logging
import os
from datetime import datetime

import apache_beam as beam

from trainer.config import PROJECT_ID, DATA_DIR, TFRECORD_DIR

logging.info('running preprocess')


def get_cloud_pipeline_options(project, output_dir):
    """Get apache beam pipeline options to run with Dataflow on the cloud

    Args:
        project (str): GCP project to which job will be submitted
        output_dir (str): GCS directory to which output will be written

    Returns:
        beam.pipeline.PipelineOptions

    """
    logging.warning('Start running in the cloud')

    options = {
        'runner': 'DataflowRunner',
        'job_name': ('ml-at-scale-flowers-{}'.format(
            datetime.now().strftime('%Y%m%d%H%M%S'))),
        'staging_location': os.path.join(output_dir, 'staging'),
        'temp_location': os.path.join(output_dir, 'tmp'),
        'project': project,
        'region': 'europe-west1',
        'zone': 'europe-west1-d',
        'autoscaling_algorithm': 'THROUGHPUT_BASED',
        'save_main_session': True,
        'setup_file': './setup.py',
    }

    return beam.pipeline.PipelineOptions(flags=[], **options)


def split(input_line):
    """Reads csv input lines and splits a line in uri and label

    Args:
        input_line (str): line in %s,%s format from csv file

    Returns:
       uri, label

    """
    uri = str(input_line.split(',')[0])
    label = str(input_line.split(',')[1])

    return {'uri': uri, 'label': label}


def main():
    """Run preprocessing as a Dataflow pipeline."""
    pipeline_options = get_cloud_pipeline_options(PROJECT_ID,
                                                  TFRECORD_DIR)

    pipeline = beam.Pipeline(options=pipeline_options)

    (pipeline | 'ReadData' >> #TODO: read flowers.csv from storage (DATA_DIR)
                              #TIP: beam.io.ReadFromText
              | 'Split' >> #TODO: apply split function to every sample
                           #TIP: beam.Map
              | 'WriteData' >> #TODO: write to bigquery table
                               #TIP: beam.io.WriteToBigQuery
     )

    pipeline.run().wait_until_finish()


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main()
