import logging
import os
from datetime import datetime

import apache_beam as beam

from trainer.config import PROJECT_ID, DATA_DIR, TFRECORD_DIR

logging.info('running preprocess')


def get_cloud_pipeline_options(project, output_dir):
    """Get apache beam pipeline options to run with Dataflow on the cloud

    Args:
        project (str): GCP project to which job will be submitted
        output_dir (str): GCS directory to which output will be written

    Returns:
        beam.pipeline.PipelineOptions

    """
    logging.warning('Start running in the cloud')

    options = {
        'runner': 'DataflowRunner',
        'job_name': ('ml-at-scale-flowers-{}'.format(
            datetime.now().strftime('%Y%m%d%H%M%S'))),
        'staging_location': os.path.join(output_dir, 'staging'),
        'temp_location': os.path.join(output_dir, 'tmp'),
        'project': project,
        'region': 'europe-west1',
        'zone': 'europe-west1-d',
        'autoscaling_algorithm': 'THROUGHPUT_BASED',
        'save_main_session': True,
        'setup_file': './setup.py',
    }

    return beam.pipeline.PipelineOptions(flags=[], **options)


def split(input_line):
    """Reads csv input lines and splits a line in uri and label

    Args:
        input_line (str): line in %s,%s format from csv file

    Returns:
       uri, label

    """
    uri = str(input_line.split(',')[0])
    label = str(input_line.split(',')[1])

    return {'uri': uri, 'label': label}


def one_hot_encoding(split_dict, all_labels):
    """Transforms label into one hot encoding array

    Args:
        split_dict (dict): dict with uri and label of sample
        all_labels: array of all labels

    Returns:
       uri, labels

    """
    uri = split_dict['uri']
    label = split_dict['label']
    index = all_labels[label]

    one_hot = [1 if i == index else 0 for i in range(len(all_labels))]

    return uri, one_hot


def main():
    """Run preprocessing as a Dataflow pipeline."""
    pipeline_options = get_cloud_pipeline_options(PROJECT_ID,
                                                  TFRECORD_DIR)

    pipeline = beam.Pipeline(options=pipeline_options)

    all_labels = (pipeline | 'ReadDictionary' >> beam.io.ReadFromText(
                                                    DATA_DIR + 'dict.txt',
                                                    strip_trailing_newlines=True)
                           | 'Split1' >> # TODO: split line on comma into tuple
                                         # TIP: use a lambda function:
                                         # lambda x: fn(x)
    )

    (pipeline | 'ReadData' >> beam.io.ReadFromText(
                                            DATA_DIR + 'flowers.csv',
                                            strip_trailing_newlines=True)
              | 'Split2' >> beam.Map(split)
              | 'OneHotEncoding' >> # TODO: apply one hot encoding with all
                                    # labels as side input
                                    # TIP: use pvalue.AsDict
    )

    pipeline.run().wait_until_finish()


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main()
