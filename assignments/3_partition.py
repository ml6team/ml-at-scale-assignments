import logging
import os
from datetime import datetime
import numpy as np

import apache_beam as beam
from apache_beam.metrics import Metrics
from tensorflow_transform import coders

from trainer.config import PROJECT_ID, DATA_DIR, TFRECORD_DIR, NUM_LABELS
from trainer.util import schema, read_image

logging.warning('running preprocess')

partition_train = Metrics.counter('partition', 'train')
partition_validation = Metrics.counter('partition', 'validation')
partition_test = Metrics.counter('partition', 'test')
examples_failed = Metrics.counter('build', 'failed')


def get_cloud_pipeline_options(project, output_dir):
    """Get apache beam pipeline options to run with Dataflow on the cloud

    Args:
        project (str): GCP project to which job will be submitted
        output_dir (str): GCS directory to which output will be written

    Returns:
        beam.pipeline.PipelineOptions

    """
    options = {
        'runner': 'DataflowRunner',
        'job_name': ('ml-at-scale-flowers-{}'.format(
            datetime.now().strftime('%Y%m%d%H%M%S'))),
        'staging_location': os.path.join(output_dir, 'staging'),
        'temp_location': os.path.join(output_dir, 'tmp'),
        'project': project,
        'region': 'europe-west1',
        'zone': 'europe-west1-d',
        'autoscaling_algorithm': 'THROUGHPUT_BASED',
        'save_main_session': True,
        'setup_file': './setup.py',
    }

    return beam.pipeline.PipelineOptions(flags=[], **options)


def split(input_line):
    """Reads csv input lines and splits a line in uri and label

    Args:
        input_line (str): line in %s,%s format from csv file

    Returns:
       uri, label

    """
    uri = str(input_line.split(',')[0])
    label = str(input_line.split(',')[1])

    return {'uri': uri, 'label': label}


def one_hot_encoding(split_dict, all_labels):
    """Transforms label into one hot encoding array

    Args:
        split_dict (dict): dict with uri and label of sample
        all_labels: array of all labels

    Returns:
       uri, labels

    """
    uri = split_dict['uri']
    label = split_dict['label']
    index = all_labels[label]

    one_hot = [1 if i == index else 0 for i in range(len(all_labels))]

    return uri, one_hot


def process_image((uri, label)):
    """Reads an image at specified uri and transforms it into pixel values

    Args:
        uri, label ((str,str))

    Returns:
       uri, label, image_bytes

    """
    image_bytes = read_image(uri)

    if image_bytes is not None:
        yield uri, label, image_bytes


def build_example((key, label, img_bytes)):
    """Build a dictionary that contains all the features and label to store
    as TFRecord

    Args:
        raw_in: raw data to build the example from

    Returns:
        dict: A dictionary of features

    """
    try:
        features = {
            'id': key,
            'label': label,
            'feat': img_bytes,
        }
        yield features

    except Exception as e:
        examples_failed.inc()
        logging.error(e, exc_info=True)
        pass


def partition_fn(example, num_partitions):
    """Deterministic partition function that partitions examples based on
    hashing.

    Args:
        example (dict): a dictionary with at least one key id

    Returns:
        int: an integer representing the partition in which the
        example is put (based on the key id)

    """
    distribution = [80, 10, 10]

    bucket = hash(str(example['id'])) % np.sum(distribution)
    # Bucket is a random number between 0 and 100

    # TODO: return 0, 1, 2 for training, validation and test set so the
    # distribution is as specified above
    # Use the partition metrics specified above to keep track of our set sizes


def main():
    """Run preprocessing as a Dataflow pipeline."""
    pipeline_options = get_cloud_pipeline_options(PROJECT_ID,
                                                  TFRECORD_DIR)

    pipeline = beam.Pipeline(options=pipeline_options)

    all_labels = (pipeline | 'ReadDictionary' >> beam.io.ReadFromText(
                                                    DATA_DIR + 'dict.txt',
                                                    strip_trailing_newlines=True)
                           | 'Split1' >> beam.Map(lambda x: x.split(',')))

    examples = (pipeline
                | 'ReadData' >> beam.io.ReadFromText(
                                            DATA_DIR + 'flowers.csv',
                                            strip_trailing_newlines=True)
                | 'Split2' >> beam.Map(split)
                | 'OneHotEncoding' >> beam.Map(one_hot_encoding,
                                             beam.pvalue.AsDict(all_labels))
                | 'ReadImage' >> # TODO: apply process_image
                                 # TIP: the function might not return anything
                | 'BuildExamples' >> #TODO: apply build_example
                                     # TIP: the function might not return
                                     #      anything

    examples_split = examples | beam.Partition(partition_fn, 3)

    example_dict = {
        'train': examples_split[0],
        'validation': examples_split[1],
        'test': examples_split[2]
    }

    train_coder = coders.ExampleProtoCoder(schema)

    for part, examples in example_dict.items():
        examples | part + '_writeExamples' >> \
            beam.io.tfrecordio.WriteToTFRecord(
                file_path_prefix=os.path.join(
                    TFRECORD_DIR, part + '_examples'),
                compression_type=beam.io.filesystem.CompressionTypes.GZIP,
                coder=train_coder,
                file_name_suffix='.tfrecord.gz')

    pipeline.run().wait_until_finish()


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main()
